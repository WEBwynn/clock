function clockRunner() {
 const clockDisplay = document.getElementById("clock-display");
 const deg = 6;
 const hour = document.querySelector('.hour'); 
 const min = document.querySelector('.min'); 
 const sec = document.querySelector('.sec');
 const updateTime = () => {
  const day = new Date();
  const hourValue = day.getHours();
  const minValue = day.getMinutes();
  const secValue = day.getSeconds();
  const p = (hourValue >= 12) ? "PM" : "AM";
  const dispayHour = (hourValue> 12) ? hourValue - 12: hourValue;
  const formttedHour = dispayHour.toString().padStart(2,"0");
  const formttedMinute = minValue.toString().padStart(2,"0");
  const formttedSecond = secValue.toString().padStart(2,"0");

  clockDisplay.textContent = `${formttedHour}:${formttedMinute}:${formttedSecond}`;

  const hh = hourValue * 30 + minValue / 2;
  const mm = minValue * deg;
  const ss = secValue * deg;

  hour.style.transform = `rotateZ(${hh}deg)`;
  min.style.transform = `rotateZ(${mm}deg)`;
  sec.style.transform = `rotateZ(${ss}deg)`;

 } 

 updateTime();
 setInterval(updateTime, 1000);

}

clockRunner();